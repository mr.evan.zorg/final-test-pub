#!/bin/bash

log="/tmp/reg.log"
echo "reg start `date`" > $log

echo $(cat /etc/hostname) > /usr/share/httpd/noindex/index.html
curl -d "name=$(cat /etc/hostname)&ip=$(awk 'END{print $1}' /etc/hosts)" -X POST http://172.17.0.20:80/api/
echo "curl is ok" >> $log

yes 'y' | ssh-keygen -q -t rsa -N '' -f /etc/ssh/ssh_host_rsa_key || echo "ssh-gen error" >> $log
echo "ssh-gen is ok" >> $log

/usr/sbin/sshd -D
echo "ssh is ok" >> $log

echo "reg stop `date`" >> $log

