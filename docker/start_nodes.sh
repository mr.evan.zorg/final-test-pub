#!/bin/bash

docker rm -f ansible-cm
docker build -t fedora-ansible:latest -f Dockerfile-ansible .
docker run -d --rm --hostname ansible-cm --name ansible-cm --ip 172.17.0.20 -p 1022:22 fedora-ansible:latest

sleep 2

docker rm -f haproxy
docker build -t finaltest-haproxy -f Dockerfile-haproxy .
docker run -d --name haproxy -p 80:80 finaltest-haproxy

docker build -t finaltest-node -f Dockerfile-nodes .
for i in 1 2 3 4 5
do
	docker rm -f node${i}
	docker run -d --hostname node${i} --name node${i} finaltest-node
	docker logs node${i}
done
