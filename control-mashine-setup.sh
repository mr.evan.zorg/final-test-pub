#!/bin/sh

apt update -y
apt install -y software-properties-common
apt-add-repository -y ppa:ansible/ansible
apt update -y
apt install -y ansible git python-pip
pip install boto

MY_USER="ubuntu"
mkdir -p /home/$MY_USER/.ssh/
mkdir -p /home/$MY_USER/.aws/

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCyFaV9jI8wSg3fhBtFtfNOdjFb5ZRzda6VxEXuF7tQ2WMGv5PS2hTNNDu2hhcT+FRQtmEeKxBISCjy+sBMLbolOa7Lvy3/p/84DklrsSTjEk0BkLHXbHGoX7fUaY1/rDBDQLgR8IhipUrrLwJv8VOVNWrHmmp0hVAsPSYc0c1jCcLZw5UVk6aa8bVFRfTu7vfmw4uXdKHoS24kSxxY/wTC+mnZcbzwph4Px2WTxLEyAUR+TQ7uAQTQEG6whKdPZOblhYmvwk7iwQRo+jHewOsv1zNbvOa7omsqsXr0oV/MEfoARdeH4Txll4V9KaCQ2NIxNj4SQbomqqlKPvQjWMEEFqi/LyjDpwouYEDjaZc1Uf8arJvo8qQLBI97XFM+pCG9QXXHeWgW9XdS6dg99ymywk7DVnyiuEOjqHwaDwxZ2lTQr/+dRLO/6lx6eEb+fynhiYZKogsd2vaMwlmWEsd4QmoJa8yTZMQ+k2rkIAdi4Jnstf+nCNlEil9dDFecqduQJ8b+tFMWhi86n78D6oauktOph5TItFyXIDa6jZ3A8CY77lm9lS1a6hnoj/sHOoVeM8IaciFmPJ8jZ8iBAXKjosoEPVmBiOdMQ+pSLr0wJ/BvKdjuNLJGmaiaUelGsSEw+ZLQgzVMG7X1StMzpAoD9Gqzc1T1om/RWGjut2eC7Q== final-test" > /home/$MY_USER/.ssh/authorized_keys

cat<<EOF>/root/.ssh/config
Host *
  CheckHostIP no
  StrictHostKeyChecking no
EOF

cat<<EOF>/home/$MY_USER/.ssh/config
Host *
  CheckHostIP no
  StrictHostKeyChecking no
EOF

cat<<EOF>/home/$MY_USER/.ssh/final-test
-----BEGIN RSA PRIVATE KEY-----
MIIJKQIBAAKCAgEAshWlfYyPMEoN34QbRbXzTnYxW+WUc3WulcRF7he7UNljBr+T
0toUzTQ7toYXE/hUULZhHisQSEgo8vrATC26JTmuy78t/6f/OA5Ja7Ek4xJNAZCx
12xxqF+31GmNf6wwQ0C4EfCIYqVK6y8Cb/FTlTVqx5pqdIVQLD0mHNHNYwnC2cOV
FZOmmvG1RUX07u735sOLl3Sh6EtuJEscWP8Ewvpp2XG88KYeD8dlk8SxMgFEfk0O
7gEE0BBusISnT2Tm5YWJr8JO4sEEaPox3sDrL9czW7zmu6JrKrF69KFfzBH6AEXX
h+E8ZZeFfSmgkNjSMTY+EkG6JqqpSj70I1jBBBaovy8ow6cKLmBA42mXNVH/Gqyb
6PKkCwSPe1xTPqQhvUF1x3loFvV3UunYPfcpssJOw1Z8orhDo6h8Gg8MWdpU0K//
nUSzv+pcenhG/n8p4YmGSqILHdr2jMJZlhLHeEJqCWvMk2TEPpNq5CAHYuCZ7LX/
pwjZRIpfXQxXnKnbkCfG/rRTFoYvOp+/A+qGrpLTqYeUyLRclyA2uo2dwPAmO+5Z
vZUtWuoZ6I/7BzqFXjPCGnIhZjyfI2fIgQFyo6LKBD1ZgYjnTEPqUi69MCfwbynY
7jSyRpmomlHpRrEhMPmS0IM1TBu19UrTM6QKA/Rqs3NU9aJv0Vho7rdngu0CAwEA
AQKCAgAt230d80/781XXNMGBFfVtQuo668dqY+dtEXmgceP+Ubw6RKLgLOAmZ0jp
G2Zd8hhkIFKp3hT3R/2jdAaIl03TwCLOvJAvwSbrXGJzpM4G2vORb50cCATMgrln
rTaOpdxXtRaxJ+s227GLFjPed5o6VeMFOzImhx26BpUF3tCwrBbM1JxTTg4c4Bj5
maYU0NxtQ/VyLspyctoN5nmKQUEMpEcVg0BQB7a+AW6auo+WfNUSYO1zRDflDBY0
xjP/MdBzUgu+gp6lrWZhUev9nFfzncrP2LjhoFVQTRuIWvW8KvThlRqHFuzijD4H
mOyL4AlgOsOJF/vDcTbes49SesxYvvFhCkEHfbyJi+fUjHDgP9iTcFKA88+MZ4uN
r40LAjDryuGoxzfLvypa01ONMI97fnayDFY6/YsmkfQMXsCIKmzYh3vSLRPoC3Gt
biOP/ZaN62q3XQWoXM+I7Q3c6Xh8PNZ/4Qrd1aoFwSKlnTS+23rYy7wfBraQPjyq
f0wKZ9iPt3s5fhgmFOSHHHvGx8+1xp/knU5nEicQG8jplYQf2EhCZCV/Yr8kFygD
MbuoxXw1/aij3OWp6ay3/zlL6jdodsBT+BnujTyp9gVR9rxE4oGGiYe1BZ0x+xUi
uj/DEv8297yFUzZawRUhFVLAECDI8pi54LWqCbFpnscScIh+0QKCAQEA5PZ3lzMf
sIAqZutvX9h7zXfInu7o9zTqbi7v4F4BFhjOLWOYQRwiGZzz48Mn6TK7vhzPVfHy
A9OuAxgOKLTHa79PNT+7rbGvBmOlNoQ4upr/LEf4zeBDi+VClkQVb52aSg/sBtWW
l+hg1ApnAlrrjnUjEe+jVhSCWDAiaLsRUPgXb6u90lh76Er+9b2H7NXIW4H3toEm
xU52MW/YgGWojhG18KhyIUaP6fgfYmAi22sQq85HHDmf1WNvvWU4ZPmE15BJ0xZS
W+bCyBgnFG3BRxnnhlhKbiESvlTzHYESDjL0DaUseR7/8clzf2IiLDXyaDkgG1wn
xZhvZUvEizAdvwKCAQEAxx0iOLl7cu2o1D0dU0PXa8Juf2rXeZNozs7mNhOKYngg
WhJAEza8JAcfCzQ0wYAYmjfkTRzvJVOPgihs8SEJ+7SWtjVjm+QyB853c598gxvY
r8F2ivlwoZdNA994Jw5apKK8QYxFqpP4wxJ5127/mZitoRNPUhWodNsOxQGrJOc9
fzTnyZ9H9SrcsBcoUoql7Dw55iWWZYbX5N7zjPsX5DuPNTbqHygpI0NOGM9XKB0a
G8jXrlm/ArXxQScar3m/QQzJCC2ad4bEyTI4wSP+VuFvn+Nx/+9R8uIY7KXlG0fj
3MQ+Cz6t3Rt/4GDJGKwv1dTKuIRxZjly0LL1wMWiUwKCAQEAoQaBpvHKiKwqpgVQ
Z9ztfNyQWM53Q1yQvvr9sxMJuoupfRx9ONVbWh4bHITk7cnDZfoHC0/EQFLAH+bs
Fk7FrY53W2C+UnMTP/AWglfNOdfGPFNFtSzS9HFBOkMZvEzf9NRrz4apiZ5hct76
ARsGbUMObg/WFLUJ0pHPsU5W6FYCzYPAVU3rnrwXwaYREUxsw4aNO2prZPpAf5ZE
clCMkZX9GgqtcZ+ZmRfqopowCZaru9iwBDa2pTFwpYepeMJPXfdZJD6fn4iUVns+
CM0D0Eh0nvQ7LGbQMtG1xXs/woKtR2dx9qEJqfde5iiTFfq+x/PdE8DlK7+AIIJ/
8M/fKwKCAQBFpoZLC28Ig+QT4hD5CHk5o7jdb3VSfOMjvLJA5lL5IQZIG6w4TGRB
wg/07l0p9vIkODrvUpjBGNrRb+yz6JfCaahysnhWltt9aZbVZku63SnOSUF7Wcgx
bmitWyV/nADPKHHsB/JyufJB4pumZZiZlRPfjY6oqxTTU7F17qB8dztG3S3blJEq
TMUIAAHF5eQmn+atACb8+s9YqzjM7ySrXL46oiwBnB3PzQJroVPen1d7sqTnw1MS
0qxC2zf1fAQP8DX+W112RDlgG+ZIgZu9rnxKlNhS69dhu/92AI4FHf+yQR9m9ZUC
DFWOGq0dqVXOWfJT21iDpsLoCEH5HekrAoIBAQDMuna2kGud34V2svpmFx//kVTp
NuZy3VrVAAJzsOwMvadaoXLSf3antjTwX3EWgD9tQ4SMsXTwtkJTCdtcP01KF6j+
81jw2OBvxcQPQj3SmMAfcH8b4ti2ZMmBbhlb/23cecXWjjugciblmMLrKmi/Uk2W
qUw0qvdVG9Ep/7ZuvPb+F3Pd4RvFf+OC4Mr9D4ULBzdwFXK9m5GgcoI2z8VPmHjQ
lJK4paNAGMbxHHTILm91frW+bKePG+eUBnZythhVO3a5cwsBI81BjlPADEIqXQaw
aiJqrQBf+L+RfW7t9blex/xP7MABh2Hey6QmFeiJqQG7Suzu4FNp6YydLo05
-----END RSA PRIVATE KEY-----
EOF

chmod 600 /home/$MY_USER/.ssh/final-test

cat<<EOF>/home/$MY_USER/.aws/credentials
[default]
#aws_access_key_id = AKIAYHPKFI3NECX7SJPB
#aws_secret_access_key = 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
aws_access_key_id = AKIAYWWSDGPZRRYABMFU
aws_secret_access_key = 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
EOF


chown -R $MY_USER:$MY_USER /home/$MY_USER/

cat<<EOF>>/home/$MY_USER/.ssh/config
Host gitlab.com
  IdentityFile /home/$MY_USER/.ssh/final-test
EOF

sudo -u $MY_USER git clone git@gitlab.com:mr.evan.zorg/final-test-ansible.git /home/$MY_USER/ansible

echo "*/2      *     *       *       *       $MY_USER    ANSIBLE_HOST_KEY_CHECKING=false /usr/bin/ansible-playbook -i /home/$MY_USER/ansible/ec2.py --limit 'tag_Ansible_Final' --private-key=/home/$MY_USER/.ssh/final-test /home/$MY_USER/ansible/play.yml -u ec2-user" >> /etc/crontab
