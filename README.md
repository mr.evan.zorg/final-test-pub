# Финальный тест по AWS


## deploy aws cloudformation
Разворачиваем инфраструктуру (VPC, Auto Scaling Group, Load Balancer, EC2) на AWS средствами cloudformation одним файлом. Среди прочих стартует контрольная машина с Ansible, находит через ec2.py-скрипт ec2 instances с тегом Ansible:Final и настраивает на них вэб сервер. Как только Load balancer видит отвечающий порт 8080, трафик начинает баланситься на эти инстансы.

На этом репозитории лежат файлы CloudFormation
```
https://github.com/alexey107/final/blob/master/final.json
```

CloudFormation код будет работать только в одном из двух регионов: US East (N. Virginia) "us-east-1" или US East (Ohio) "us-east-2"
```
aws cloudformation deploy --template-file ./final.json --stack-name final-work-kabanov-sysoev --parameter-overrides KeyName=final
```

## Репозиторий html (публичный)
С этого репозитория backend забирает веб-странички
```
https://gitlab.com/mr.evan.zorg/final-test-www
```

## Репозиторий ansible (приватный)
key: "final-test"

С этого репозитория контрольная машина забирает Ansible playbook
```
git@gitlab.com:mr.evan.zorg/final-test-ansible.git
```

## S3 backet с картинками (публичный)
Отсюда nginx проксирует картинки
```
https://final-test-kabanov.s3.amazonaws.com/
```


## Пояснение к "Финальный тест по AWS":
В скрипте control-mashine-setup.sh (который используется в cloud formation json файле) содержится private rsa key, я понимаю что так быть не должно, однако делая так я руководствовался принципом минимальных итераций. Это значит что я хотел представить как можно скорее рабочий код, чтобы получить от преподавателя разъяснения и задачи на правки и затем привести его к более оптимальному виду. Но так сложилось что это задание у нас так и не проверили, курсы закончились, преподаватели занялись другими более преоритетными задачами, поэтому код остался в таком виде.
Также там есть пара aws_access_key_id + aws_secret_access_key, но это менее критично, т.к. этот ключ имеет (имел, он уже не работает) доступ только на чтение и только для S3 bucket.